import React, {useState} from 'react';
import { TextField, Button, FormControl, FormLabel, RadioGroup, FormControlLabel, Radio } from "@material-ui/core";
import './style.scss';


function FormController(props) {
    const [lan, setLan] = useState('')
    const [lag, setLag] = useState('')
    const [locationName, setLocationName] = useState('')
    const [byName, setByName] = useState('byCoord')

    const handleSubmit=()=>{
        if(byName=== 'byCoord'){
            if(!lan.length || !lag.length){
                // will handle error with error state on text field && return from method
            }
            props.submitCord([lan, lag])
        }else {
            if(!locationName.length){
                // will handle error with error state on text field && return from method
            }
            props.submitName({name:locationName})
        }
    }

    const generateCordInput = ()=>{
        return (
            <form className="" noValidate autoComplete="off">
                <TextField
                    onChange={(e)=>setLan(Number(e.target.value))}
                    label="Latitude"
                    variant="outlined" />
                <TextField
                    onChange={(e)=>setLag(Number(e.target.value))}
                    label="Longitude"
                    variant="outlined" />
            </form>
        )
    }


    const generateNameInput = ()=>{
        return (
            <form className="" noValidate autoComplete="off">
                <TextField
                    onChange={(e)=>setLocationName(e.target.value)}
                    label="Location Name"
                    variant="outlined" />
            </form>
        )
    }
    return (
        <div className="controller_wrapper">
            <FormControl component="fieldset">
                <FormLabel component="legend">Gender</FormLabel>
                <RadioGroup name="gender1" value={byName} onChange={(e)=>setByName(e.target.value)}>
                    <FormControlLabel value="byCoord" control={<Radio />} label="By Coordinations" />
                    <FormControlLabel value="byName" control={<Radio />} label="By Name" />
                </RadioGroup>
            </FormControl>
            { byName=== 'byCoord' && generateCordInput()}
            { byName=== 'byName' && generateNameInput()}

            <Button variant="contained" color="primary" onClick={handleSubmit}>
                Add Marker
            </Button>
        </div>
    );
}

export default FormController;
