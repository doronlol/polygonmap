import React, { useState} from 'react';
import './style.scss';
import Map from '../../components/Map'
import FormController from './FormController'

function Home() {
    const [coordinations, setCoordinations] = useState([])
    const [placeName, setPlaceName] = useState('')

    return (
        <div className="home_wrapper">
            <h1>Polygon Map </h1>
            <div className="map_controller">
                <FormController submitCord={setCoordinations} submitName={setPlaceName}/>
                <Map coordinations={coordinations} placeName={placeName}/>
            </div>

        </div>
    );
}

export default Home;
