import React, {useEffect, useState} from 'react';
import './style.scss';
import { ReactBingmaps } from 'react-bingmaps';
import helper from '../../services/helper'


function Map(props) {
    const [pushPins, setPushpins] = useState([]);
    const [mapCenter, setMapCenter] = useState([13.0827, 80.2707]);


    // add new pins to list
    useEffect(()=>{
        if(props.coordinations.length){
            setPushpins([...pushPins, props.coordinations])
        }
    },[props.coordinations])


    useEffect(()=>{
        if(pushPins.length === 1){
            setMapCenter(pushPins[0])
        }
    },[pushPins])

    const generatePins = ()=>{
        return pushPins.map(item=>{
            return {
                "location":item, "option":{ color: 'red' }, "addHandler": {"type" : "click" }
            }
        })
    }

    const generatePolygon = ()=>{
        if(pushPins.length < 3){
            return []
        }
        const {x,y,r} = helper.circleFromThreePoints(...pushPins);
        return [
            {
                "center":[x, y],
                "radius":r *100,
                "points":3,
                "option": {fillColor: "green", strokeThickness: 2}
            }
        ]
    }


    return (
        <div className="map_wrapper">
            <ReactBingmaps
                bingmapKey ={process.env.REACT_APP_BING_API_KEY}
                center = {mapCenter}
                pushPins = {generatePins()}
                regularPolygons = {generatePolygon()}

            >
            </ReactBingmaps>
        </div>
    );
}

export default Map;
